﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{

    public GameObject ButtonPanel;
    public GameObject InstructionPanel;

    public void PlayGame(){
        SceneManager.LoadScene("SampleScene");
    }

    public void QuitGame(){
        Application.Quit();
    }

    public void Back(){
        InstructionPanel.SetActive(false);
        ButtonPanel.SetActive(true);
    }

    public void Instruction(){
        InstructionPanel.SetActive(true);
        ButtonPanel.SetActive(false);
    }
}
