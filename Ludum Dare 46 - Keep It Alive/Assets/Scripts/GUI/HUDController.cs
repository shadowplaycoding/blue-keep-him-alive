﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUDController : MonoBehaviour
{

    #region 
    public static HUDController instance;

    private void Awake() {
        instance = this;
    }
    #endregion

    public GameObject GameOverGUI;
    public GameObject ScoreGUI;
    public GameObject WinGUI;

    public TMPro.TextMeshProUGUI _score;
    public TMPro.TextMeshProUGUI _goScore;
    public TMPro.TextMeshProUGUI _winScore;

    // Start is called before the first frame update
    void Start()
    {
        GameOverGUI.SetActive(false);
        WinGUI.SetActive(false);
    }

    public void UpdateScoreText(string score){
        _score.SetText($"{score}");
        _goScore.SetText($"Score: {score}");
        _winScore.SetText($"Your Score: {score}");
    }

}
