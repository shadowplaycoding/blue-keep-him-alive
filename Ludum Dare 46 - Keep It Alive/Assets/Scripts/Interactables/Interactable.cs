﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    [Range(10,1000)]
    public int score = 100;

    public virtual void Interact(){
        addScore();
        //Debug.Log("Interacting with " + transform.name + " score increased by " + score + " to " + GameController.instance.score);
    }

    void addScore(){
        GameController.instance.AddScore(score);
    }
}
