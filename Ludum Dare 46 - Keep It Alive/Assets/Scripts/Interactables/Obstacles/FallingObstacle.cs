﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FallingObstacle : Obstacle
{   
    Rigidbody2D rb;

    GameObject characterObject;

    private void Awake() {
        isTriggered = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        StartCoroutine(CheckForTrigger(this.transform));
    }

    public override void Trigger(){
        base.Trigger();
        float distanceToChar = ObstacleTrigger.DistanceToCharacter(this.transform);
        float distanceToGround = ObstacleTrigger.DistanceToGround(this.transform);

        if(distanceToChar < (characterDistanceFactor * distanceToGround)){
            rb.gravityScale = 1;
            triggered = true;
        }
    }
}
