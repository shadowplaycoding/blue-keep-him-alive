﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : Interactable
{

    public GameObject changeTo;

    [Header("Trigger Settings")]
    public bool isTriggered = false;
    public bool triggered = false;

    [Range(0.1f,1f)]
    public float characterDistanceFactor = 0.455f;

    public override void Interact(){
        base.Interact();
        Instantiate(changeTo, transform.position, transform.rotation, transform.parent);
        Destroy(gameObject);
        
    }

    public virtual void Trigger(){
        
    }

    public IEnumerator CheckForTrigger(Transform objectTransform){
        while(triggered == false){

            Trigger();
            if(triggered == true){
                yield break;
            }
            yield return new WaitForSeconds(0.25f);
        }
    }

}
