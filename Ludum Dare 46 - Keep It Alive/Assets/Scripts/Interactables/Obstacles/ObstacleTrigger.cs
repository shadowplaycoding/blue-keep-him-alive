﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTrigger
{
    public static float DistanceToCharacter(Transform objectTransform){
        float cx = CharacterControl.instance.transform.position.x;
        float ox = objectTransform.position.x;

        float distance = Mathf.Abs(ox-cx);

        return distance; 
    }

    public static float DistanceToGround(Transform objectTransform){
        float gy = GroudController.instance.transform.position.y;
        float oy = objectTransform.position.y;

        float distance = Mathf.Abs(oy-gy);
        return distance;
    }

    bool TriggerAtDistanceToCharacter(float distanceToTrigger, float distanceToCharacter){

        if(distanceToTrigger < distanceToCharacter){
            return true;
        }
        else{
            return false;
        }
    }

}
