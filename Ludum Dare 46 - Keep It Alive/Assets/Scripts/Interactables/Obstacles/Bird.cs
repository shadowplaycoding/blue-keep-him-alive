﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MovingObstacle
{

    public GameObject poopObject;

    float distanceToGround;

    private void Awake() {
        isTriggered = true;
    }

    private void Start() {
        StartCoroutine(CheckForTrigger(this.transform));
    }

    public override void Trigger(){
        base.Trigger();

        float distanceToCharacter = ObstacleTrigger.DistanceToCharacter(this.transform);
        float distanceToGround = ObstacleTrigger.DistanceToGround(this.transform);

        if(distanceToGround > 5f && distanceToCharacter < characterDistanceFactor * distanceToGround){
            Instantiate(poopObject,transform.position,transform.rotation);
            triggered = true;
        }
        else if(distanceToGround < 5f){
            triggered = true;
        }
    }

}
