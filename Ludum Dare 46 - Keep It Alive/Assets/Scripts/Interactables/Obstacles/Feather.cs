﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Feather : MonoBehaviour
{

    Rigidbody2D rb;
    Renderer ren;

    private void Start() {
        ren = GetComponent<Renderer>();
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
    }

    void Update(){
        if(ren.isVisible){
            rb.gravityScale = 1;
        }
    }

    private void OnCollisionEnter(Collision other) {
        if(other.collider.tag == "Safe"){
            Destroy(gameObject);
        }
    }
}
