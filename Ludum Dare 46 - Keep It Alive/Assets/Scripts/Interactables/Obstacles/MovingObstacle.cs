﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : Obstacle
{

    public float speed = 2f;

    Renderer ren;

    private void Start() {
        ren = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Renderer>().isVisible){
            transform.Translate(-speed * Time.deltaTime, 0f, 0f);
        }
    }
}
