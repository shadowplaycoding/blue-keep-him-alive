﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{

    public static CharacterControl instance;

    private void Awake() {
        instance = this;
    }

    GameController gameController;

    // Start is called before the first frame update
    void Start()
    {
        gameController = GameController.instance;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(gameController.speed * Time.deltaTime, 0f, 0f);
    }

    private void OnCollisionEnter2D(Collision2D other) {

        if(other.collider.tag != "Safe" && other.collider.tag != "Goal"){
            gameController.speed = 0f;

            // Call Game Over GUI
            HUDController.instance.ScoreGUI.SetActive(false);
            HUDController.instance.GameOverGUI.SetActive(true);
        }
        else if (other.collider.tag == "Goal"){
            // Call Game Win
            gameController.speed = 0f;
            HUDController.instance.ScoreGUI.SetActive(false);
            HUDController.instance.WinGUI.SetActive(true);
        }    
    }
}
