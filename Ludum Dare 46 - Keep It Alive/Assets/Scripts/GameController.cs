﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    #region 
    public static GameController instance;

    void Awake() {
        instance = this;
    }
    #endregion

    public float speed = 2f;

    public int score = 0;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)){
            MouseHandler();
        }
    }

    void MouseHandler(){
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePosition.x,mousePosition.y);

        RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

        if(hit.collider != null){
            Interactable interactable = hit.collider.GetComponent<Interactable>();
            if(interactable != null){
                interactable.Interact();
            }
        }
    }

    public void AddScore(int scoreToAdd){
        score += scoreToAdd;
        HUDController.instance.UpdateScoreText(score.ToString());
    }

    public void RestartGame(){
        SceneManager.LoadScene("SampleScene");
    }

    public void QuitGame(){
        Application.Quit();
    }
}
